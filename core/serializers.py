from rest_framework import serializers
from .models import Setting, TempStatistic, Devices, User

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'phone', 'middle_name']

class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Devices
        exclude = ['token', 'activated']

class SettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Setting
        exclude = ['user', 'device']

class SettingDeviceSerializer(serializers.ModelSerializer):
    device = DeviceSerializer()
    class Meta:
        model = Setting
        fields = ['device', 'name', 'id']
        depth = 2

class TempStatisticSerializer(serializers.ModelSerializer):
    class Meta:
        model = TempStatistic
        exclude = ['device', 'id']
