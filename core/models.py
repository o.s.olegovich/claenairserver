from django.db import models
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db.models.signals import post_save
from django.utils.timezone import now as nowtime
import uuid
import random, string


def randomString(stringLength=64):
    """Generate a random string of letters, digits and special characters """
    password_characters = string.ascii_letters + string.digits
    return ''.join(random.choice(password_characters) for i in range(stringLength))


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, phone, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not phone:
            raise ValueError('The given phone must be set')

        user = self.model(phone=phone, **extra_fields)
        user.is_active = False
        if user.is_superuser is True:
            user.is_active = True
            user.is_staff = True
        user.set_password(password)
        user.save(using=self._db)
        # sendConfirmSMS(user)

        return user

    def create_user(self, phone, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(phone, password, **extra_fields)

    def create_superuser(self, phone, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone, password, **extra_fields)


class User(AbstractUser):
    phone = models.CharField(max_length=12, unique=True, verbose_name='Номер телефона',
                             validators=[
                                 RegexValidator(regex='^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$',
                                                message="Номер должен быть в формате +79999999999")])

    middle_name = models.CharField(max_length=255, verbose_name="Отчество", blank=True, null=True)
    username = models.CharField(max_length=255, verbose_name='Имя пользователя', blank=True, unique=False)
    email = models.EmailField(verbose_name="Email", blank=True, unique=False)
    token = models.CharField(max_length=255, verbose_name="Токен", default=randomString)

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name', 'middle_name']

    objects = UserManager()

    class Meta:
        verbose_name = 'Пользватель'
        verbose_name_plural = 'Пользватели'


# class Type(models.Model):
#     CA01 = 'ca01'
#     CA02 = 'ca02'
#
#     STYLES =((CA01,CA01), (CA02,CA02))
#     # help_text = "БЕЗ ПРОБЕЛОВ НА АНГЛИСКОМ (пример CA-01)"
#     style = models.CharField(max_length=16, primary_key=True, verbose_name="Скорощенное название модели",
#                              choices=STYLES)
#     name = models.CharField(max_length=255, verbose_name="Название модели на русском")
#
#     def __str__(self):
#         return self.name
#
#     class Meta:
#         verbose_name = "Модель устройства"
#         verbose_name_plural = "Модели устройств"


class Devices(models.Model):
    CA01 = 'ca01'
    CA02 = 'ca02'
    STYLES = ((CA01, "Рекуператор CA-01"), (CA02, "Рекуператор CA-02"))

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, verbose_name="Уникальный идентификатор")
    serial_number = models.BigIntegerField(verbose_name="Серийный номер")
    token = models.TextField(verbose_name="Токен доступа", default=randomString)
    style = models.CharField(verbose_name="Модель", max_length=255, choices=STYLES)
    activated = models.BooleanField(verbose_name='Устройство активировано', default=False)

    def __str__(self):
        return str(self.serial_number) + " " + self.style

    class Meta:
        verbose_name = "Устройство"
        verbose_name_plural = "Устройства"


class Setting(models.Model):
    id = models.BigAutoField(primary_key=True)

    device = models.OneToOneField(Devices, verbose_name="Устройство", on_delete=models.CASCADE, related_name="settings")
    user = models.ForeignKey(User, verbose_name="Пользватель", on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name="Имя", blank=True)

    auto_is_on = models.BooleanField(default=True, verbose_name="Автоматический режим по CO2")

    inside_speed = models.PositiveSmallIntegerField(default=0, verbose_name="Скорость притока воздуха")
    outside_speed = models.PositiveSmallIntegerField(default=0, verbose_name="Скорость оттока воздуха")
    heater_is_on = models.BooleanField(verbose_name="Нагреватель включен", default=False)

    night_is_on = models.BooleanField(default=False, verbose_name="Включать ночной режим")
    night_auto_is_on = models.BooleanField(default=True, verbose_name="Автоматический ночной режим по CO2")
    night_time_on = models.TimeField(verbose_name="Время включения ночного режима", blank=True, null=True)
    night_time_off = models.TimeField(verbose_name="Время выключения ночного режима", blank=True, null=True)
    night_inside_speed = models.PositiveSmallIntegerField(default=0,
                                                          verbose_name="Скорость притока воздуха ночной режим")
    night_outside_speed = models.PositiveSmallIntegerField(default=0,
                                                           verbose_name="Скорость оттока воздуха ночной режим")

    timer_is_on = models.BooleanField(verbose_name="Таймер активирован", default=False)

    timer_on = models.DateTimeField(null=True, default=None, verbose_name="Время включения (таймер)", blank=True)

    timer_off = models.DateTimeField(null=True, default=None, verbose_name="Время отключения (таймер)", blank=True)

    timer_inside_speed = models.PositiveSmallIntegerField(default=0,
                                                          verbose_name="Скорость притока воздуха (таймер)")
    timer_outside_speed = models.PositiveSmallIntegerField(default=0,
                                                           verbose_name="Скорость оттока воздуха (таймер)")
    timer_heater_is_on = models.BooleanField(verbose_name="Нагреватель для таймера включен", default=False)

    def __str__(self):
        return str(self.device.serial_number) + " " + self.device.style

    class Meta:
        verbose_name = "Настройки устройства "
        verbose_name_plural = "Настройки устройств"


def post_save_setting(sender, instance, **kwargs):
    if instance.heater_is_on is True and (instance.inside_speed == 0 or instance.outside_speed == 0):
        instance.heater_is_on = False
        instance.save()
    if instance.timer_heater_is_on is True and (instance.timer_inside_speed == 0 or instance.timer_outside_speed == 0):
        instance.timer_heater_is_on = False
        instance.save()


post_save.connect(post_save_setting, sender=Setting)


class TempStatistic(models.Model):
    id = models.BigAutoField(primary_key=True)
    device = models.ForeignKey(Setting, on_delete=models.CASCADE)
    co2 = models.PositiveSmallIntegerField()

    inside_pressure = models.PositiveSmallIntegerField()
    inside_temp = models.DecimalField(max_digits=5, decimal_places=2)
    inside_humidity = models.DecimalField(max_digits=6, decimal_places=2)

    outside_pressure = models.PositiveSmallIntegerField()
    outside_temp = models.DecimalField(max_digits=5, decimal_places=2)
    outside_humidity = models.DecimalField(max_digits=6, decimal_places=2)

    datetime = models.DateTimeField(default=nowtime)

    class Meta:
        ordering = ['-datetime']
        verbose_name = "Статистика"
        verbose_name_plural = "Статистика"


# Create your models here.

class AddRecupToken(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.TextField(default=randomString,)
