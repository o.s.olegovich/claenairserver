from django.shortcuts import render
import json
from .models import Setting, Devices, TempStatistic, AddRecupToken
from django.http import HttpResponse, JsonResponse
from .serializers import SettingSerializer
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.timezone import now as timenow
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def get_set(request):
    if request.method == "POST":
        from_device = json.loads(request.body.decode('utf-8'))
        devices = Devices.objects.filter(token=from_device['token'])
        if devices.exists():
            setting = Setting.objects.filter(device__id=devices[0].id)[0]
            serializer = SettingSerializer(setting)
            return JsonResponse(serializer.data, status=200)
        else:
            return HttpResponse(status=404)


# Create your views here.

@csrf_exempt
def statistics(request):
    if request.method == "POST":
        from_device = json.loads(request.body.decode('utf-8'))
        devices = Devices.objects.filter(token=from_device['token'])
        if devices.exists():
            setting = Setting.objects.filter(device__id=devices[0].id)[0]
            temp_stat = TempStatistic(device=setting, co2=from_device['co2'],
                                      inside_pressure=from_device['inside_pressure'],
                                      inside_humidity=from_device['inside_humidity'],
                                      inside_temp=from_device['inside_temp'],
                                      outside_pressure=from_device['outside_pressure'],
                                      outside_humidity=from_device['outside_humidity'],
                                      outside_temp=from_device['outside_temp'])
            temp_stat.save()
            return JsonResponse({'OK': 'OK'})

        else:
            return HttpResponse(status=404)


@csrf_exempt
def add_setting(request):
    if request.method == "POST":
        from_device = json.loads(request.body.decode('utf-8'))
        devices = Devices.objects.filter(token=from_device['token'])
        token_add = AddRecupToken.objects.filter(token=from_device['add_token'])
        if token_add.exists() and token_add.exists():
            settings = Setting.objects.filter(device=devices[0])
            if settings.exists():
                settings.delete()
            setting = Setting(user=token_add.user[0], device=devices[0])
            setting.save()
            token_add.delete()
            return JsonResponse({'OK': 'OK'})
        return HttpResponse(status=404)
    return HttpResponse(status=404)


@csrf_exempt
def get_time(request):
    if request.method == "POST":
        time_dict = {'time': timenow()}
        return HttpResponse(json.dumps(time_dict, cls=DjangoJSONEncoder))
