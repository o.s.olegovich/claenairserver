from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from .models import TempStatistic, Devices, Setting, User, AddRecupToken

from django.contrib.auth.models import AbstractUser

admin.site.site_header = 'Clean Air'  # default: "Django Administration"
admin.site.index_title = 'Админка'  # default: "Site administration"
admin.site.site_title = 'Clean Air | Админка'  # default: "Django site admin"


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class CustomUserAdmin(UserAdmin):
    form = CustomUserChangeForm
    search_fields = ('phone', 'email', 'first_name', 'last_name',)

    fieldsets = (
        (None, {'fields': ('phone', 'username', 'password')}),
        ('Персональная информация',
         {'fields': ('last_name', 'first_name', 'middle_name', 'email',)}),
        (
            'Права доступа',
            {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions', 'token',)}),
        ('Важные даты', {'fields': ('last_login', 'date_joined',)})
    )
    add_fieldsets = ((None, {'fields': ('phone', 'password1', 'password2', 'username',),
                             'classes': ('wide',)}),)
    list_display = ('phone', 'email', 'first_name', 'last_name', 'is_staff',)


# class TypeAdmin(admin.ModelAdmin):
#     list_display = ('style', 'name')


class SettingAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('id', 'name', 'is_night', 'inside_speed', 'outside_speed')


class DevicesAdmin(admin.ModelAdmin):
    list_display = ('serial_number', 'style', 'activated')
    search_fields = ('serial_number', 'style')


class TempStatisticAdmin(admin.ModelAdmin):
    readonly_fields = ('datetime',)
    list_display = ('id', 'device', 'datetime')

class  AddRecupTokenAdmin(admin.ModelAdmin):
    list_display = ('token', 'user', 'id')


# Register your models here.
admin.site.register(AddRecupToken, AddRecupTokenAdmin)
admin.site.register(Setting, SettingAdmin)
admin.site.register(Devices, DevicesAdmin)
admin.site.register(TempStatistic, TempStatisticAdmin)
admin.site.register(User, CustomUserAdmin)
# admin.site.register(Type, TypeAdmin)