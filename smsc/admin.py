from django.contrib import admin
from . import models

class smsAdmin(admin.ModelAdmin):
    list_display = ("phoneNumber", 'code', "userid", "isconfirmed", 'datetime')



admin.site.register(models.sms, smsAdmin)