from .models import sms
from . import smsc_api
import logging
logger = logging.getLogger(__name__)

def sendConfirmSMS(user):
    SMS = sms(phoneNumber=user.phoneNumber, userid=user.id)
    SMS.save()
    SMSC = smsc_api.SMSC()
    response = SMSC.send_sms(phones=user.phoneNumber, format=9, message="code", id=SMS.id)
    if response[1] != "1":
        logger.error("Error SMSC:" + str(response))
        return False
    else:
        SMS.code = response[4][-4:]
        print("Код: " +str(SMS.code))
        SMS.save()
        return True