from django.db import models
from django.core.validators import RegexValidator



class sms(models.Model):
    id = models.AutoField(primary_key=True, verbose_name="id")
    phoneNumber = models.CharField(max_length=12, unique=False, verbose_name='Номер телефона',
                                   validators=[
                                       RegexValidator(regex='^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$',
                                                      message="Номер должен быть в формате +79999999999")])
    code = models.CharField(verbose_name="Код", max_length=4, blank=True, null=True)
    isconfirmed = models.BooleanField(default=False, verbose_name="Подтверждено")
    userid = models.IntegerField(verbose_name="Пользователь", unique=False, blank=True, null=True)
    datetime = models.DateTimeField(auto_now_add=True, verbose_name='Время отправки запроса')
    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name = 'СМС'
        verbose_name_plural = 'СМС'
