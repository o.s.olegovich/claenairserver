from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'smsc'
    verbose_name = "СМС"
