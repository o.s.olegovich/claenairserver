from core.models import User, randomString
import json


def set_token(id):
    user = User.objects.get(id=id)
    token = randomString()
    user.token = token
    user.save()
    return token


def is_token_auth(token):
    return User.objects.filter(token=token).exists()


def token_auth(token):
    if is_token_auth(token=token):
        return User.objects.get(token=token)
    else:
        return None


def token_auth_data(body):
    data = json.loads(body)
    user = token_auth(data['token'])
    return data, user


def auth_user(phone, password):
    users = User.objects.filter(phone=phone)
    print(phone)
    print(password)
    if users.exists() is True:
        user = users[0]
        if user.check_password(password) is True:
            return set_token(user.id)
        else:
            return None
    else:
        return None


from decimal import Decimal
from django.core.serializers.json import DjangoJSONEncoder


class DecimalEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return DjangoJSONEncoder.default(self, obj)
