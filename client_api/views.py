import json

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.timezone import now as timenow
from django.utils.timezone import timedelta
from core.models import Devices, User, Setting, TempStatistic, AddRecupToken
from core.serializers import SettingSerializer, DeviceSerializer, UserProfileSerializer, SettingDeviceSerializer, \
    TempStatisticSerializer
from .utils import is_token_auth, auth_user, token_auth_data, DecimalEncoder


def only_post(*args, **kwargs):
    def actual_decorator(func):
        def wrapper(request, *args, **kwargs):
            if request.method == "POST":
                return func(request, *args, **kwargs)
            else:
                return HttpResponse(status=404)

        return wrapper

    return actual_decorator


def only_auth(*args, **kwargs):
    def actual_decorator(func):
        def wrapper(request, *args, **kwargs):
            # print(request.body)
            data = json.loads(request.body)

            if is_token_auth(data['token']) is True:
                return func(request, *args, **kwargs)
            else:
                return HttpResponse(status=401)

        return wrapper

    return actual_decorator


@csrf_exempt
@only_post()
def register(request):
    data = json.loads(request.body)
    phone = data['phone']
    password = data['password']
    id = data['uuid']
    if Devices.objects.filter(id=id).exists():
        if User.objects.filter(phone=phone).exists():
            return JsonResponse({'success': False, 'mes': "Такой пользватель уже существует"})
        user = User.objects.create_user(phone=phone, password=password, is_active=False)
        return JsonResponse({'success': True})
    else:
        return JsonResponse({'success': False}, status=403)


@csrf_exempt
@only_post()
def register_confirm(request):
    data = json.loads(request.body)
    phone = data['phone']
    users = User.objects.filter(phone=phone)
    if users.exists():
        user = users[0]
        user.is_active = True
        user.save()
        return JsonResponse({'success': True, 'token': user.token})
    else:
        return JsonResponse({'success': False}, status=403)


@csrf_exempt
@only_post()
def auth(request):
    data = json.loads(request.body)
    print(data)
    token = auth_user(password=data['password'], phone=data['phone'])
    if token != None:
        return JsonResponse({'token': token})
    else:
        return HttpResponse(status=403)


@csrf_exempt
@only_post()
@only_auth()
def get_recuperators(request):
    data, user = token_auth_data(request.body)
    recups = Setting.objects.filter(user=user)
    serializer = SettingDeviceSerializer(recups, many=True)
    data = serializer.data
    data_json = json.dumps(data)
    return HttpResponse(data_json)


@csrf_exempt
@only_post()
@only_auth()
def add_recuperator(request):
    data, user = token_auth_data(request.body)
    recups = AddRecupToken.objects.filter(user=user)
    if recups.exists():
        recup = recups[0]
    else:
        recup = AddRecupToken(user=user)
        recup.save()

    data_json = json.dumps({'add_token': recup.token})
    return HttpResponse(data_json)


@csrf_exempt
@only_post()
@only_auth()
def get_settings(request):
    data, user = token_auth_data(request.body)
    recups = Setting.objects.get(user=user, device__id=data['id'])
    serializer = SettingSerializer(recups)
    return JsonResponse(serializer.data)


@csrf_exempt
@only_post()
@only_auth()
def set_settings(request):
    data, user = token_auth_data(request.body)
    setting = Setting.objects.get(user=user, device__id=data['id'])
    for (key, value) in data.items():

        if key != 'uuid' and key != 'token' and key != 'user' and key != 'device' and key != 'id' and key != 'timer_minutes':
            setattr(setting, key, value)

        elif key == 'timer_minutes':
            setting.timer_is_on = True
            setting.timer_on = timenow()
            setting.timer_off = timenow() + timedelta(minutes=data['timer_minutes'])

    setting.save()
    serializer = SettingSerializer(setting)
    return JsonResponse(serializer.data)


@csrf_exempt
@only_post()
@only_auth()
def get_profile(request):
    data, user = token_auth_data(request.body)
    serializer = UserProfileSerializer(user)
    return JsonResponse(serializer.data)


@csrf_exempt
@only_post()
@only_auth()
def set_profile(request):
    data, user = token_auth_data(request.body)
    elements = ('email', 'first_name', 'last_name', 'middle_name')
    for key, value in data.items():
        if key in elements:
            setattr(user, key, value)

    user.save()
    serializer = UserProfileSerializer(user)
    return JsonResponse(serializer.data)


@csrf_exempt
@only_post()
@only_auth()
def get_stat(request):
    data, user = token_auth_data(request.body)
    stat = TempStatistic.objects.filter(device__id=data['id'], device__user=user).order_by('-datetime').first()
    serializer = TempStatisticSerializer(stat)
    data = serializer.data
    data_json = json.dumps(data, cls=DecimalEncoder)
    return HttpResponse(data_json)


@csrf_exempt
@only_post()
@only_auth()
def get_stats(request):
    data, user = token_auth_data(request.body)
    if data['value'] in ['co2', 'inside_pressure', 'inside_temp', 'inside_humidity', 'outside_pressure',
                         'outside_temp', 'outside_humidity']:
        if data['days'] == 1:
            stats = list(TempStatistic.objects.filter(device__id=data['id'], device__user=user,
                                                      datetime__gte=timenow() - timedelta(hours=24)).values_list(
                data['value'], 'datetime'))
        else:
            stats = list(TempStatistic.objects.filter(device__id=data['id'], device__user=user,
                                                      datetime__gte=timenow() - timedelta(
                                                          days=data['days'])).values_list(
                data['value'], 'datetime'))
        stats_json = json.dumps(stats, cls=DecimalEncoder)
        return HttpResponse(stats_json)
    else:
        return HttpResponse(status=404)

    # stats = TempStatistic.objects.filter(device__id=data['id'], device__user=user).order_by('-datetime').v

    # serializer = TempStatisticSerializer(stat)
    # data = serializer.data
    # data_json = json.dumps(data)
    # return HttpResponse(data_json)


# @csrf_exempt
# @only_post()
# @only_auth()
# def set_timer(request):
#     data, user = token_auth_data(request.body)
#     settings = Setting.objects.filter(device__id=data['id'])
#     if settings.exists():
#         setting = settings[0]
#         for (key, value) in data.items():
#             if key != 'uuid' and key != 'token' and key != 'user' and key != 'device' and key != 'id' and key != 'minutes':
#                 setvalue
#                 pass
#     return JsonResponse()


@csrf_exempt
@only_post()
@only_auth()
def del_recup(request):
    data, user = token_auth_data(request.body)
    recups = Setting.objects.filter(user=user, id=data['id'])
    recups.delete()
    return HttpResponse(status=200)


@csrf_exempt
@only_post()
@only_auth()
def add_recups(request):
    data, user = token_auth_data(request.body)
    if AddRecupToken.objects.filter(user).exists():
        recup_add = AddRecupToken.objects.filter(user)[0]
    else:
        recup_add = AddRecupToken(user=user)
        recup_add.save()

    return JsonResponse({'add_token': recup_add.token})
