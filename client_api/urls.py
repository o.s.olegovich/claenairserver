from django.urls import path, include
from .views import auth, register, register_confirm, get_recuperators, get_settings, set_settings, get_profile, \
    set_profile, get_stat, get_stats, add_recuperator, del_recup


urlpatterns = [
    path('auth/', auth),
    path('register/', register),
    path('register/confirm/', register_confirm),
    path('recuperators/', get_recuperators),
    path('recuperators/add/', add_recuperator),
    path('recuperators/del/', del_recup),
    path('setting/get/', get_settings),
    path('setting/set/', set_settings),
    path('profile/set/', set_profile),
    path('profile/get/', get_profile),
    path('stat/', get_stat),
    path('stats/', get_stats),
]