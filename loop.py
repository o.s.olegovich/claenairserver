import os, machine, time
from machine import Pin, RTC
import ureq as requests
import _thread
from drivers import fans, co2, bme280_float
import btree, machine, time

rtc = machine.RTC()
# reset_button = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
#
# def reset():
#     time.sleep(3)
#     if not reset_button.value():
#         try:
#             os.remove('config.db')
#         except:
#             pass
#         machine.reset()

i2c = machine.I2C(scl=Pin(22), sda=Pin(21))
bme = bme280_float.BME280(i2c=i2c)
bme2 = bme280_float.BME280(i2c=i2c, address=0x77)


def to_datetime(time):
    t = time
    t = t.replace('T', '-')
    t = t.replace(':', '-')
    t_ar = t.split('.')[0].split('-')
    print(t_ar)
    t_ar.append('0')
    t_ar.append('0')
    print(t_ar)
    t_ar = list(map(int, t_ar))
    return tuple(t_ar)


def set_time():
    r = requests.post("http://89.108.103.74/device/time/", timeout=2.0)
    resp_json = r.json()
    t = resp_json['time']
    t = t.replace('T', '-')
    t = t.replace(':', '-')
    t_ar = t.split('.')[0].split('-')
    print(t_ar)
    t_ar.append('0')
    t_ar.append('0')
    print(t_ar)
    t_ar = list(map(int, t_ar))
    print(t_ar)
    rtc.init(tuple(t_ar))
    print(rtc.datetime())


def check_system():
    f = open('setting.json', 'r')
    json_setting = f.read()
    f.close()
    resp_json = json.loads(json_setting)

    if rtc.datetime() > to_datetime(resp_json['timer_on']) and rtc.datetime() < to_datetime(resp_json['timer_off']):
        fans.inside_fan(resp_json['timer_inside_speed'])
        fans.outside_fan(resp_json['timer_outside_speed'])
        fans.heater(resp_json['timer_heater_is_on'])

    if resp_json['auto_is_on'] == False:
        fans.inside_fan(resp_json['inside_speed'])
        fans.outside_fan(resp_json['outside_speed'])
        fans.heater(resp_json['heater_is_on'])

    else:
        # print(co2.co2())
        if co2.co2() < 400:
            fans.inside_fan(0)
            fans.outside_fan(0)

        elif co2.co2() > 600 and co2.co2() < 800:
            fans.inside_fan(1)
            fans.outside_fan(1)

        elif co2.co2() > 800:
            fans.inside_fan(2)
            fans.outside_fan(2)


def send_stat():
    while True:

        try:
            data = {'inside_temp': int(bme2.values[0][:-1].split('.')[0]),
                    'inside_pressure': int(bme2.values[1][:-3].split('.')[0]),
                    'inside_humidity': int(bme2.values[2][:-1].split('.')[0]),
                    'outside_temp': int(bme.values[0][:-1].split('.')[0]),
                    'outside_pressure': int(bme.values[1][:-3].split('.')[0]),
                    'outside_humidity': int(bme.values[2][:-1].split('.')[0]), 'token': "peTyHoDkwwuA9x4nRaZK",
                    'co2': co2.co2(),
                    }
            r = requests.post("http://89.108.103.74/device/statistics/", json=data,
                              timeout=2.0)
            r.close()
            # print(resp_json)

        except:
            pass
        time.sleep(120)
        set_time()


def _loop():
    time.sleep(1)
    try:
        r = requests.post("http://89.108.103.74/device/sets/", json={'token': "peTyHoDkwwuA9x4nRaZK"}, timeout=5.0)
    except:
        return
    resp_json = r.json()
    r.close()
    file = open('setting.json', 'w')
    file.write(str(resp_json))
    file.close()

    if resp_json['auto_is_on'] == False:
        fans.inside_fan(resp_json['inside_speed'])
        fans.outside_fan(resp_json['outside_speed'])
        fans.heater(resp_json['heater_is_on'])

    else:
        # print(co2.co2())
        if co2.co2() < 400:
            fans.inside_fan(0)
            fans.outside_fan(0)

        elif co2.co2() > 600 and co2.co2() < 800:
            fans.inside_fan(1)
            fans.outside_fan(1)

        elif co2.co2() > 800:
            fans.inside_fan(2)
            fans.outside_fan(2)


# ('31.12C' d[0][:-1] , '1010.95hPa' d[0][:-3], '43.93%')

# inside_pressure = from_device['inside_pressure'],
# inside_humidity = from_device['inside_humidity'],
# inside_temp = from_device['inside_temp'],
# outside_pressure = from_device['outside_pressure'],
# outside_humidity = from_device['outside_humidity'],
# outside_temp = from_device['outside_temp'])

# try:
#     f = open("config.db", "r+b")
# except OSError:
#     f = open("config.db", "w+b")
#
# db = btree.open(f)
# d
# print(resp_json)


# if not reset_button.value():
#     reset()


def loop():
    while True:
        _loop()
        check_system()


def start():
    time.sleep(3)
    set_time()
    _thread.start_new_thread(loop, ())
    _thread.start_new_thread(send_stat, ())
    _thread.start_new_thread(check_system, ())


